package my.html.parser.service.impl;

import my.html.parser.repositories.StatisticsRepository;
import my.html.parser.repositories.UniqueWordRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class HtmlServiceImplTest {

    @InjectMocks
    private HtmlServiceImpl htmlService;

    @Mock
    private StatisticsRepository statisticsRepository;

    @Mock
    private UniqueWordRepository uniqueWordRepository;


    @Test
    void test_calculateUniqueWords() throws IOException {
        HtmlServiceImpl mockService = Mockito.spy(htmlService);
        List<String> words = new ArrayList<>();
        words.add("проект");
        words.add("программирование");
        words.add("программирование");
        words.add("проект");
        words.add("ПО");
        words.add("задачи");
        words.add("ПО");
        Mockito.doReturn(words).when(mockService).htmlToListWords(any());
        Mockito.doNothing().when(mockService).saveStatisticsInRepo(any(), any());
        Map<String, Integer> erUniqueWords = new HashMap<>();
        erUniqueWords.put("проект", 2);
        erUniqueWords.put("программирование", 2);
        erUniqueWords.put("ПО", 2);
        erUniqueWords.put("задачи", 1);

        Map<String, Integer> arUniqueWords = mockService.calculateUniqueWords("https://www.simbirsoft.com/");

        Assertions.assertEquals(erUniqueWords, arUniqueWords);
    }
}
