package my.html.parser;

import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import my.html.parser.model.StatisticsDto;
import my.html.parser.service.HtmlService;
import my.html.parser.model.ApiError;
import my.html.parser.model.ValidationError;
import org.hibernate.validator.constraints.URL;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@AllArgsConstructor
@RestController
@RequestMapping("html")
@Validated
public class HtmlController {

    private final HtmlService service;

    @GetMapping("/statistics/unique-words")
    public Map<String, Integer> statistic(@RequestParam("url")
                                          @NotNull @NotEmpty @URL String url) throws IOException {
        return service.calculateUniqueWords(url);
    }

    @GetMapping("/statistics/find/all")
    public List<StatisticsDto> findStatistics() {
        return service.getAllStatistic();
    }

    @GetMapping("/statistics/find/url")
    public StatisticsDto findStatistics(@RequestParam("url")
                                        @NotNull @NotEmpty @URL String url) throws NotFoundException {
        return service.getStatistic(url);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public List<ValidationError> handleMethodArgumentNotValidException(ConstraintViolationException ex) {
        log.error(ex);
        return ex.getConstraintViolations().stream()
                .map(constraintViolation -> {
                    String field = constraintViolation.getPropertyPath().toString().replaceAll(".+\\.", "");
                    return new ValidationError(field, constraintViolation.getMessage());
                }).collect(Collectors.toList());
    }

    @ExceptionHandler(UnknownHostException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleUnknownHostExceptionException(UnknownHostException ex) {
        log.error(ex);
        String message = String.format("%s, данный url не найден.", ex.getMessage());
        return new ApiError(HttpStatus.BAD_REQUEST.value(), message);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleMissingServletRequestParameterExceptionException(MissingServletRequestParameterException ex) {
        log.error(ex);
        return new ApiError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiError handleMNotFoundException(NotFoundException ex) {
        log.error(ex);
        return new ApiError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    }
}
