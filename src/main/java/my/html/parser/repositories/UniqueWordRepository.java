package my.html.parser.repositories;

import my.html.parser.model.UniqueWords;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UniqueWordRepository extends JpaRepository<UniqueWords, Integer> {
}
