package my.html.parser.repositories;

import my.html.parser.model.Statistics;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StatisticsRepository extends JpaRepository<Statistics, Integer> {

    Optional<Statistics> findByUrl(String url);
}
