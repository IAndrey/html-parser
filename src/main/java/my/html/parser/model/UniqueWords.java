package my.html.parser.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UniqueWords {
    @Id
    @GeneratedValue
    private int id;

    @Column(name = "unique_word")
    private String uniqueWord;

    @Column
    private int count;

    @ManyToOne
    private Statistics statistics;

    public UniqueWords(String uniqueWord, int count, Statistics statistics) {
        this.uniqueWord = uniqueWord;
        this.count = count;
        this.statistics = statistics;
    }
}
