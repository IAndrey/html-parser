package my.html.parser.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class StatisticsDto {

    private final String url;
    private final Map<String, Integer> uniqueWords;

    public static StatisticsDto convertToStatisticsDto(Statistics statistics) {
        Map<String, Integer> uniqueWords = statistics.getUniqueWords().stream().collect(Collectors.toMap(
                UniqueWords::getUniqueWord,
                UniqueWords::getCount
        ));
        return new StatisticsDto(statistics.getUrl(), uniqueWords);
    }
}
