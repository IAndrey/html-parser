package my.html.parser.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Statistics {

    @Id
    @GeneratedValue
    private int id;

    @Column
    private String url;

    @OneToMany(mappedBy = "statistics")
    private List<UniqueWords> uniqueWords;

    public Statistics(String url) {
        this.url = url;
    }
}
