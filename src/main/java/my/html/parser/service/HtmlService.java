package my.html.parser.service;

import javassist.NotFoundException;
import my.html.parser.model.StatisticsDto;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface HtmlService {

    Map<String, Integer> calculateUniqueWords(String url) throws IOException;

    StatisticsDto getStatistic(String url) throws NotFoundException;

    List<StatisticsDto> getAllStatistic();
}
