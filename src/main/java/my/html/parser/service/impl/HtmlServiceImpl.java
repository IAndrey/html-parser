package my.html.parser.service.impl;

import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import my.html.parser.model.Statistics;
import my.html.parser.model.StatisticsDto;
import my.html.parser.model.UniqueWords;
import my.html.parser.repositories.StatisticsRepository;
import my.html.parser.repositories.UniqueWordRepository;
import my.html.parser.service.HtmlService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class HtmlServiceImpl implements HtmlService {

    private final StatisticsRepository statisticsRepository;
    private final UniqueWordRepository uniqueWordRepository;


    public Map<String, Integer> calculateUniqueWords(String url) throws IOException {
        Document doc = Jsoup.connect(url).get();
        List<String> words = htmlToListWords(doc);
        Map<String, Integer> uniqueWords = words.stream().distinct().collect(Collectors.toMap(
                word -> word,
                word -> (int) words.stream().filter(str -> str.equals(word)).count()
        ));
        saveStatisticsInRepo(url, uniqueWords);
        return uniqueWords;
    }

    public List<String> htmlToListWords(Document doc) {
        return doc.body().getAllElements().stream()
                .map(Element::text)
                .flatMap(str -> Stream.of(str.split("[ \n\"«».:;,?\\[\\]()'!]")))
                .filter(word -> word.length() > 1 && word.matches("\\D.+"))
                .collect(Collectors.toList());
    }

    public void saveStatisticsInRepo(String url, Map<String, Integer> uniqueWords) {
        Statistics statistics = statisticsRepository.save(new Statistics(url));
        List<UniqueWords> words = uniqueWords.keySet().stream().map(key ->
                new UniqueWords(key, uniqueWords.get(key), statistics))
                .collect(Collectors.toList());
        words.forEach(uniqueWordRepository::save);
    }

    public StatisticsDto getStatistic(String url) throws NotFoundException {
        Statistics statistics = statisticsRepository.findByUrl(url).orElseThrow(() ->
                new NotFoundException(url + ", для адреса не найдена статистика."));
        return StatisticsDto.convertToStatisticsDto(statistics);
    }

    public List<StatisticsDto> getAllStatistic() {
        List<Statistics> statistics = statisticsRepository.findAll();
        return statistics.stream().map(StatisticsDto::convertToStatisticsDto).collect(Collectors.toList());
    }
}
